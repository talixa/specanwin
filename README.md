# C#-based Spectrum Analyzer #

This is a port of my Java spectrum analyzer project to a Windows native client. This code was written using the community version of Visual Studio 2015. The original project is available at https://bitbucket.org/talixa/specan/overview. This version does not have all the features of the Java version.

# Currently Working For #
* 8K 16-bit PCM wave files

# Functionality Includes #
* Spectrum Analyzer
* Waterfall Display
* Oscilloscope