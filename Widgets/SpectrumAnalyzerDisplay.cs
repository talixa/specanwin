﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using SpectrumAnalyzer.Source;
using SpectrumAnalyzer.Source.FFT;
using NAudio.Wave;

namespace SpectrumAnalyzer.Widgets
{
    public partial class SpectrumAnalyzerDisplay : UserControl
    {
        private const int RESOLUTION = 512;          // length/width of widget
        private const int ZOOM = 1024;               // magnification factor for spectrum
        private const int FFT_LEN = RESOLUTION * 2;  // Length of FFT buffer ***MUST be a power of 2***	
        private const int shiftValue = 800;          // number of bytes to shift for a refresh rate of 1/10 second

        // Frequency data
        private Complex[] amplitudeData = new Complex[FFT_LEN];                 // amplitude data from PCM
        private Complex[] frequencyData;                                        // corresponding frequency data
        private double[] scaledAmpFreq;                                         // amp/freq data
        private double[][] waterfallData;                                       // waterfall data		

        private WaveFileReader waveFile;          // wave file to analyze

        // loop audio?
        private bool loop = false;
        private bool soundOn = true;

        // things to display
        private bool isWaterfall = false;      // true if waterfall to display
        private bool isScope = false;          // true if scope to display
        private int scopeCenter;                // center point for oscilloscope view
        private int waterfallRows;              // number of rows of waterfall to display
        private static int scopeZoom = 1;       // zoom factor for oscope

        // data for label
        private int clickedFreq = 0;            // last clicked frequency
        private int maxFreq = 0;                // maximum frequency in current spectrum display
        private int freqSpan = 0;               // frequency span for clicked points

        // for drawing span line
        private int startX = 0;                 // click point x
        private int startY = 0;                 // click point y
        private int currX = 0;                  // current mouse position
        private int currY = 0;                  // current mouse position

        // points for previous line
        private int lastX1 = 0;
        private int lastX2 = 0;
        private int lastY1 = 0;
        private int lastY2 = 0;

        // colors
        private Brush BACKGROUND = Brushes.Black;
        private Pen SPECTRUM = Pens.Green;
        private Pen LINE = Pens.Red;

        // status labels to update
        private ToolStripStatusLabel lblMax;
        private ToolStripStatusLabel lblCursor;
        private ToolStripStatusLabel lblSpan;
        private String displayMode;

        public SpectrumAnalyzerDisplay()
        {
            InitializeComponent();
            InitializeDataBuffer();
            
            // this prevents flicker
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
        }        

        private void InitializeDataBuffer()
        {
            waterfallData = new double[RESOLUTION / 2][];

            // C# arrays are different than java = init second order arrays here
            for (int i = 0; i < waterfallData.Length; ++i)
            {
                waterfallData[i] = new double[RESOLUTION];
            }
        }

        // This is the audio file to play. Unfortunately, C# 
        // provides very limited functionality for playback
        private SoundPlayer clip;

        public void setWave(String fileName)
        {            
            // read wave file     
            try
            {
                waveFile = new WaveFileReader(fileName);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid audio file", "Error");
                waveFile = null;
            }

            if (waveFile == null)
            {
                return;
            }

            // play audio file
            try
            {
                if (soundOn)
                {
                    clip = new SoundPlayer(fileName);
                    clip.Play();                   
                }
            }
            catch (Exception)
            {
                // ignore errors
            }
            this.isPlaying = true;
        }

        public void setMaxLabel(ToolStripStatusLabel lblMax)
        {
            this.lblMax = lblMax;
        }

        public void setSpanLabel(ToolStripStatusLabel lblSpan)
        {
            this.lblSpan = lblSpan;
        }

        public void setCursorLabel(ToolStripStatusLabel lblCursor)
        {
            this.lblCursor = lblCursor;
        }

        private void safeSetLabel(ToolStripStatusLabel lbl, String value)
        {
            if (lbl != null)
            {
                lbl.Text = value;
            }
        }

        //*********************************************************
        //				DRAW DATA ON SCREEN
        //*********************************************************
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);            

            // get graphics object           
            Graphics g = e.Graphics;

            // set background
            g.FillRectangle(Brushes.Black, 0, 0, RESOLUTION, RESOLUTION);

            // update status labels
            safeSetLabel(lblMax, "Max: " + maxFreq);
            safeSetLabel(lblCursor, "Cursor: " + clickedFreq);
            safeSetLabel(lblSpan, "Span: " + freqSpan);            

            // if nothing to display, return
            if (scaledAmpFreq == null)
            {
                return;
            }
          
            // display oscope data
            if (isScope)
            {
                int lastSample = (amplitudeData.Length / 2) / scopeZoom;

                for (int i = 1; i < lastSample; ++i)
                {
                    int currentDataPoint = (int)(amplitudeData[i].getReal() * 100) + (scopeCenter);
                    int previousDataPoint = (int)(amplitudeData[i - 1].getReal() * 100) + (scopeCenter);
                    int previousX = (i - 1) * scopeZoom;
                    int currentX = i * scopeZoom;
                    g.DrawLine(SPECTRUM, previousX, previousDataPoint, currentX, currentDataPoint);
                }
            }

            // display waterfall data
            if (isWaterfall)
            {
                Pen p = new Pen(Color.Red);
                for (int row = 0; row < waterfallRows; ++row)
                {
                    for (int col = 0; col < RESOLUTION - 1; ++col)
                    {
                        if (waterfallData[row] != null)
                        {
                            // This implementation for color works very well. 						
                            int re = (int)(waterfallData[row][col] * 100000);
                            int gr = (int)(waterfallData[row][col] * 10000);
                            int bl = (int)(waterfallData[row][col] * 1000);
                            
                            p.Color = Color.FromArgb(re > 255 ? 255 : re, gr > 255 ? 255 : gr, bl > 255 ? 255 : bl);
                            g.DrawRectangle(p, col, row, 1, 1);                            
                        }
                    }
                }
                p.Dispose();
            }

            // always display spec an
            for (int i = 0; i < scaledAmpFreq.Length; ++i)
            {
                g.DrawLine(SPECTRUM, i, RESOLUTION, i, RESOLUTION - (int)(scaledAmpFreq[i] * ZOOM));
            }

            // if one click, display line from click to current mouse
            // else if previous clicks, display line from click 1 to click 2
            if (startX > 0 && startY > 0)
            {
                g.DrawLine(LINE, startX, startY, currX, currY);
            }
            else if (lastX1 > 0 && lastY1 > 0)
            {
                g.DrawLine(LINE, lastX1, lastY1, lastX2, lastY2);             
            }
        }

        //***********************************************************
        //						GUI THREAD
        //***********************************************************
        private static BackgroundWorker worker = null;

        private bool terminateWorker = false;
        public void runSpectrumAnalysis()
        {
            // if already running, kill thread 
            if (worker != null)
            {
                terminateWorker = true;     // flag the FFT to stop             
                Thread.Sleep(500);          // give FFT thread time to stop
                terminateWorker = false;    // reset flag
                worker.Dispose();           // cleanup worker
                InitializeDataBuffer();     // re-initialize data buffer
            }

            // create worker thread and run transform
            worker = new BackgroundWorker();
            worker.DoWork += doInBackground;
            worker.RunWorkerAsync();
        }
     
        private void doInBackground(object sender, DoWorkEventArgs e)
        {
            bool terminated = false;
            do
            {
                try
                {                                    
                    short[] samples = SharedDSPFunctions.extractWaveFileData(waveFile);
                    runTransform(samples);                                       
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    terminated = true;
                }
            } while (loop && !terminated);
        }

        //*****************************************************
        //					FFT CODE
        //*****************************************************	
        // iterate through data and calculate FFT data
        private void runTransform(short[] samples)
        {
            int numberSamples = samples.Length;
            for (int baseAddress = 0; (baseAddress) + FFT_LEN <= numberSamples; baseAddress += shiftValue)
            {
                for (int offset = 0; offset < FFT_LEN; offset++)
                {
                    short signedSample = samples[baseAddress + offset];
                    double sample = ((double)signedSample) / short.MaxValue;
                    amplitudeData[offset] = new Complex(sample, 0);
                }

                while (!isPlaying)
                {
                    Thread.Sleep(100);
                }
                long startTime = CurrentTimeMillis();
                runFFT();
                long endTime = CurrentTimeMillis();

                // this will cause the screen to repaint
                this.Invalidate();                                

                // 1/10 of a second = realtime for an 8K sampled signal w/ a shift of 1600 bytes (2 bits / sample, 800 samples)				
                // subtract the time to run the FFT so that the audio stays relatively synced
                int sleepTime = (int)(100 - (endTime - startTime));
                Thread.Sleep(sleepTime);

                // exit if we are trying to kill the worker thread that runs this
                if (terminateWorker)
                {
                    break;
                }
            }
        }

        // Run the FFT against the amplitude data 
        private void runFFT()
        {
            frequencyData = FFT.fft(amplitudeData);

            // move waterfall data
            for (int i = (RESOLUTION / 2) - 1; i > 0; --i)
            {
                waterfallData[i] = waterfallData[i - 1];
            }

            scaledAmpFreq = new double[FFT_LEN / 2];

            double fmax = -99999.9;
            double fmin = 99999.9;

            // Use FFT_LEN/2 since the data is mirrored within the array.
            for (int i = 1; i < FFT_LEN / 2 - 1; i++)
            {
                double re = frequencyData[i].getReal();
                double im = frequencyData[i].getImaginary();
                //get amplitude and scale to range 0 - RESOLUTION
                scaledAmpFreq[i] = FrequencyTools.amplitudeScaled(re, im, FFT_LEN, RESOLUTION);
                if (scaledAmpFreq[i] > fmax)
                {
                    fmax = scaledAmpFreq[i];
                    maxFreq = SharedDSPFunctions.getFreqBySampleNumber(FFT_LEN, i);
                }
                else if (scaledAmpFreq[i] < fmin)
                {
                    fmin = scaledAmpFreq[i];
                }
            }

            // set top row of waterfall
            waterfallData[0] = scaledAmpFreq;
        }

        //**********************************************************
        //				EXTERNAL INTERFACE OPTIONS
        //**********************************************************	
        // state goes from spectrum -> waterfall -> scope -> all -> back to spectrum
        public void toggleSecondView()
        {
            if (isWaterfall && !isScope)
            {
                isWaterfall = false;
                isScope = true;
                scopeCenter = RESOLUTION / 4;
                displayMode = "Spectrum + Scope";
            }
            else if (isScope && !isWaterfall)
            {
                isWaterfall = true;
                isScope = true;
                scopeCenter = RESOLUTION / 2;
                waterfallRows = RESOLUTION / 4;
                displayMode = "Display All";
            }
            else if (isScope && isWaterfall)
            {
                isWaterfall = false;
                isScope = false;
                displayMode = "Spectrum Only";
            }
            else
            {
                isWaterfall = true;
                isScope = false;
                waterfallRows = RESOLUTION / 2;
                displayMode = "Spectrum + Waterfall";
            }
            Invalidate();
        }

        public String getDisplayMode()
        {
            return displayMode;
        }

        public void toggleLoop()
        {
            loop = !loop;
            Invalidate();
        }

        public bool isLooping()
        {
            return loop;
        }

        private bool isPlaying = true;         

        public void play()
        {
            isPlaying = true;

            // Sound player does not support pausing or resuming
            /*
            if (clip != null)
            {                
                clip.Play();
            }
            */
        }

        public void stop()
        {
            isPlaying = false;

            // we can stop playback, but resume is not possible
            if (clip != null)
            {
                clip.Stop();
            }
        }

        private float gain;

        public void toggleSound()
        {
            if (clip != null)
            {
                /*
                FloatControl volume = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
                if (soundOn)
                {
                    gain = volume.getValue();
                    volume.setValue(-80);
                }
                else if (!soundOn)
                {
                    volume.setValue(gain);
                }
                */
            }
            soundOn = !soundOn;
            Invalidate();
        }

        public bool isSoundOn()
        {
            return soundOn;
        }

        public void increaseZoom()
        {
            scopeZoom *= 2;
        }

        public void decreaseZoom()
        {
            if (scopeZoom > 1)
            {
                scopeZoom /= 2;
            }
        }

        public bool isScopeDisplayed()
        {
            return this.isScope;
        }

        //***************************************************
        //			ALL BELOW CODE HANDLES MOUSE EVENTS
        //***************************************************
        private void mouseMoved(object Sender, MouseEventArgs e)
        {
            clickedFreq = SharedDSPFunctions.getFreqBySampleNumber(FFT_LEN, e.X);
            currX = e.X;
            currY = e.Y;
            Invalidate();
        }

        private int startSpan;
        private void mousePressed(object Sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // left button = freq span
                if (startX == 0)
                {
                    startX = e.X;
                    startY = e.Y;
                    startSpan = SharedDSPFunctions.getFreqBySampleNumber(FFT_LEN, e.X);
                }
                else
                {
                    freqSpan = Math.Abs(SharedDSPFunctions.getFreqBySampleNumber(FFT_LEN, e.X) - startSpan);

                    // persist current line
                    lastX1 = startX;
                    lastY1 = startY;
                    lastX2 = e.X;
                    lastY2 = e.Y;

                    // reset start
                    startX = 0;
                    startY = 0;
                }
            }
            else
            {
                // right button = clear
                startX = 0;
                startY = 0;
                lastX1 = 0;
                lastY1 = 0;
                lastX2 = 0;
                lastY2 = 0;
                freqSpan = 0;
            }
            Invalidate();
        }

        public static long CurrentTimeMillis()
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
            return (long)javaSpan.TotalMilliseconds;
        }
    }
}
