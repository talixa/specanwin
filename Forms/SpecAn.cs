﻿using System;
using System.Windows.Forms;

namespace SpectrumAnalyzer
{
    public partial class SpecAn : Form
    {       
        public SpecAn()
        {
            InitializeComponent();

            this.spectrumDisplay.setMaxLabel(lblMax);
            this.spectrumDisplay.setSpanLabel(lblSpan);
            this.spectrumDisplay.setCursorLabel(lblCursor);     
        }

        private void menuFileExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuHelpAbout_Click(object sender, EventArgs e)
        {
            About abt = new About();
            abt.ShowDialog();
        }

        private void menuFileOpen_Click(object sender, EventArgs e)
        {
            FileDialog fd = new OpenFileDialog();
            DialogResult res = fd.ShowDialog();
            if (res == DialogResult.OK)
            {
                spectrumDisplay.stop();
                spectrumDisplay.setWave(fd.FileName);
                spectrumDisplay.runSpectrumAnalysis();
                btnPause.Enabled = true;
                btnPlay.Enabled = false;
            }
        }

        private void btnSound_Click(object sender, EventArgs e)
        {           
            spectrumDisplay.toggleSound();
            updateFlags();
        }

        private void btnLoop_Click(object sender, EventArgs e)
        {
            spectrumDisplay.toggleLoop();
            updateFlags();
        }        

        private void btnZoomIn_Click(object sender, EventArgs e)
        {
            spectrumDisplay.increaseZoom();
        }

        private void btnZoomOut_Click(object sender, EventArgs e)
        {
            spectrumDisplay.decreaseZoom();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            spectrumDisplay.play();
            btnPause.Enabled = true;
            btnPlay.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            spectrumDisplay.stop();
            btnPause.Enabled = false;
            btnPlay.Enabled = true;
        }

        private void btnDisplay_Click(object sender, EventArgs e)
        {
            spectrumDisplay.toggleSecondView();
            updateFlags();
        }

        private void updateFlags()
        {
            bool sound = spectrumDisplay.isSoundOn();
            bool loop = spectrumDisplay.isLooping();

            if (sound && loop)
            {
                lblFlags.Text = "Loop, Sound";
            }
            else if (sound)
            {
                lblFlags.Text = "Sound";
            }
            else if (loop)
            {
                lblFlags.Text = "Loop, Mute";
            }
            else
            {
                lblFlags.Text = "Mute";
            }

            lblDisplayMode.Text = spectrumDisplay.getDisplayMode();
        }
    }
}
