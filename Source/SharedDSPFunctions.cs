﻿using System;
using NAudio.Wave;

namespace SpectrumAnalyzer.Source
{
    class SharedDSPFunctions
    {
        public static double SAMPLE_RATE = 8000;
              
        public static short[] extractWaveFileData(WaveFileReader waveFile)
        {
            byte[] rawData = new byte[waveFile.Length];
            waveFile.Read(rawData, 0, (int)waveFile.Length);

            short[] data = new short[waveFile.Length/2];
            for (int i = 0; i < data.Length; ++i)
            {
                data[i] = BitConverter.ToInt16(rawData, i * 2);
            }
            return data;
        }

        public static int getFreqBySampleNumber(int fftLength, int sampleNumber)
        {
            return (int)((SAMPLE_RATE / fftLength) * sampleNumber);
        }

        public static int getSampleNumberByFrequency(int fftLength, double freq)
        {
            return (int)Math.Pow((SAMPLE_RATE / (fftLength * freq)), -1);
        }
    }
}
