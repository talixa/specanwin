﻿using System;
using System.Linq;

/*************************************************************************
 * This is standard FFT code copied from a Java function.
 *
 *  Compute the FFT and inverse FFT of a length N complex sequence.
 *  Bare bones implementation that runs in O(N log N) time. Our goal
 *  is to optimize the clarity of the code, rather than performance.
 *
 *  Limitations
 *  -----------
 *   -  assumes N is a power of 2
 *
 *   -  not the most memory efficient algorithm (because it uses
 *      an object type for representing complex numbers and because
 *      it re-allocates memory for the subarray, instead of doing
 *      in-place or reusing a single temporary array)
 *  
 *************************************************************************/
namespace SpectrumAnalyzer.Source.FFT
{
    class FFT
    {
        public static Complex[] fft(Complex[] x)
        {
            int N = x.Count();

            // base case
            if (N == 1) return new Complex[] { x[0] };

            // radix 2 Cooley-Tukey FFT
            if (N % 2 != 0) { throw new Exception("N is not a power of 2"); }

            // fft of even terms
            Complex[] even = new Complex[N / 2];
            for (int k = 0; k < N / 2; k++)
            {
                even[k] = x[2 * k];
            }
            Complex[] q = fft(even);

            // fft of odd terms
            Complex[] odd = even;  // reuse the array
            for (int k = 0; k < N / 2; k++)
            {
                odd[k] = x[2 * k + 1];
            }
            Complex[] r = fft(odd);
            odd = null;
            // combine
            Complex[] y = new Complex[N];
            for (int k = 0; k < N / 2; k++)
            {
                double kth = -2 * k * Math.PI / N;

                Complex wk = new Complex(Math.Cos(kth), Math.Sin(kth));
                y[k] = q[k].plus(wk.times(r[k]));
                y[k + N / 2] = q[k].minus(wk.times(r[k]));
            }
            return y;
        }

        // compute the inverse FFT of x[], assuming its length is a power of 2
        public static Complex[] ifft(Complex[] x)
        {
            int N = x.Count();
            Complex[] y = new Complex[N];

            // take conjugate
            for (int i = 0; i < N; i++)
            {
                y[i] = x[i].conjugate();
            }

            // compute forward FFT
            y = fft(y);

            // take conjugate again
            for (int i = 0; i < N; i++)
            {
                y[i] = y[i].conjugate();
            }

            // divide by N
            for (int i = 0; i < N; i++)
            {
                y[i] = y[i].times(1.0 / N);
            }

            return y;
        }

        // compute the circular convolution of x and y
        public static Complex[] cconvolve(Complex[] x, Complex[] y)
        {

            // should probably pad x and y with 0s so that they have same length
            // and are powers of 2
            if (x.Count() != y.Count()) { throw new Exception("Dimensions don't agree"); }

            int N = x.Count();

            // compute FFT of each sequence
            Complex[] a = fft(x);
            Complex[] b = fft(y);

            // point-wise multiply
            Complex[] c = new Complex[N];
            for (int i = 0; i < N; i++)
            {
                c[i] = a[i].times(b[i]);
            }

            // compute inverse FFT
            return ifft(c);
        }

        // compute the linear convolution of x and y
        public static Complex[] convolve(Complex[] x, Complex[] y)
        {
            Complex ZERO = new Complex(0, 0);

            Complex[] a = new Complex[2 * x.Count()];
            for (int i = 0; i < x.Count(); i++) a[i] = x[i];
            for (int i = x.Count(); i < 2 * x.Count(); i++) a[i] = ZERO;

            Complex[] b = new Complex[2 * y.Count()];
            for (int i = 0; i < y.Count(); i++) b[i] = y[i];
            for (int i = y.Count(); i < 2 * y.Count(); i++) b[i] = ZERO;

            return cconvolve(a, b);
        }
    }
}
